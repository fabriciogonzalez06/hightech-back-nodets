import {Router,Request,Response} from 'express';
import {body as validatorBody, check , validationResult} from 'express-validator';

import { ResponseHttp } from '../helpers/response';
import { returnValidatorErrors } from '../utils/errorsExpressValidatorCustom';
import {saveNumbers,getNumbers } from  '../controllers/numerosController';


const api:Router= Router();

api.get('/:id',   async (req:Request,res:Response)=>{
    
    try {
        const {id} = req.params;

        if(!id){
            const resHttp=  ResponseHttp.returnCusmtomErrorV2('id  property is required in params ');
            return res.status(resHttp.httpCode).send(resHttp);
        }
       
        const result = getNumbers(id);
         res.status(result.httpCode).send(result);

    } catch (error) {
        const  errorResult =  ResponseHttp.returnErrorInternal(error.message);
        res.status(errorResult.httpCode).send(errorResult);
    }

});

api.post('/', 

validatorBody('numeros').notEmpty().withMessage('property is required')
,async (req:Request,res:Response)=>{
    try {
        

        const errors= validationResult(req);
        
        if(errors["errors"].length>0){
            const resError= returnValidatorErrors(errors);
            return res.status(resError.httpCode).send(resError);
        }

        const {numeros} = req.body;
       
        const result = saveNumbers(numeros);
         res.status(result.httpCode).send(result);

    } catch (error) {
        const  errorResult =  ResponseHttp.returnErrorInternal(error.message);
        res.status(errorResult.httpCode).send(errorResult);
    }
});


export default api;
import { Router, Request, Response } from 'express';
import { body as validatorBody, check, validationResult } from 'express-validator';

import { ResponseHttp } from '../helpers/response';
import { returnValidatorErrors } from '../utils/errorsExpressValidatorCustom';
import EmployeController from './../controllers/employeesController';


const api: Router = Router();

api.get('/:id', async (req: Request, res: Response) => {

    try {

        const { id} =req.params;
        if(!id){
            const errorR= ResponseHttp.returnCusmtomErrorV2('id query params is required');
            return res.status(errorR.httpCode).send(errorR);
        }
        const employeController = new EmployeController();

        const result = await employeController.getEmployById(id);
        res.status(result.httpCode).send(result);

    } catch (error) {
        const errorResult = ResponseHttp.returnErrorInternal(error.message);
        res.status(errorResult.httpCode).send(errorResult);
    }

});

api.post('/',

    check('nombre').notEmpty().withMessage('property is required'),
    check('correo').notEmpty().withMessage('property is required'),
    check('telefono').notEmpty().withMessage('property is required'),
    check('puesto').notEmpty().withMessage('property is required'),
    check('edad').notEmpty().withMessage('property is required'),
    check('notas').notEmpty().withMessage('property is required')
    , async (req: Request, res: Response) => {
        try {


            const errors = validationResult(req);
            if (errors["errors"].length>0) {
                const resError = returnValidatorErrors(errors);;
                return res.status(resError.httpCode).send(resError);
            }
            const params = req.body;
            const employeController = new EmployeController();

            const result = await employeController.save(params);
            res.status(result.httpCode).send(result);

        } catch (error) {
            const errorResult = ResponseHttp.returnErrorInternal(error.message);
            res.status(errorResult.httpCode).send(errorResult);
        }
    });


export default api;
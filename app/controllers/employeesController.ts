import DbHelper from "../helpers/db";
import { IResponse, ResponseHttp } from "../helpers/response";

export default class Employees {


    async getEmployById(id:number | any): Promise<IResponse> {
        const dbhelper = new DbHelper();
        try {

            return await dbhelper.executeSp('call tblempleados_obtener(?) ', [id]);

        } catch (error) {

            return ResponseHttp.returnErrorInternal(error.message);
        } finally {
            dbhelper.closePool();
        }
    }

    async save(params: any) {

        const dbhelper = new DbHelper();

        try {
            let sendParams: any[] = [];
            if (params) {
                sendParams = [
                    params.nombre,
                    params.correo,
                    params.telefono,
                    params.puesto,
                    params.edad,
                    params.notas
                ];
            }

            return await dbhelper.executeSp(' call tblempleado_insertar(?,?,?,?,?,?)', sendParams);

        } catch (error) {
            return ResponseHttp.returnErrorInternal(error.message);
        } finally {
            dbhelper.closePool();
        }
    }

}
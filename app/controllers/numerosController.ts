import { IResponse, ResponseHttp } from "../helpers/response";
import {v4 as uuidV4} from 'uuid';


const REQUESTS:any[]=[];


export const saveNumbers=(stringNumbers:string ):IResponse=>{
 
    if(stringNumbers.trim().length===0){
        return ResponseHttp.returnCusmtomErrorV2('empty string ');
    }
    
    try {
        
        const numbers:any[]= stringNumbers.split(',');
        const id:any= uuidV4() ;
        const response:IResponse={
            data:[{id}],
            errors:[],
            existsError:false,
            httpCode:200,
            message:"everything ok",
            messageErrorInternal:null
    
        }
        let  request:{id:any,numbers:any[]}={id,numbers:[]};
        
        numbers.forEach(n=>{
            if(!isNaN(n)){
                request.numbers.push({value:n,description: n % 2 ? 'impar' :'par'});
            }
        })
        
        if(REQUESTS.length>=3){
            REQUESTS.pop();
        }
        REQUESTS.unshift(request);
    
        return response;
    } catch (error) {
        return ResponseHttp.returnErrorInternal(error.message);
    }


    
}

export const getNumbers=(id:any):IResponse=>{



    const response= new ResponseHttp();

    const numberRequest = REQUESTS.filter(r=>r.id===id);

    response.httpCode=200;
    response.existsError=false;
    response.message= numberRequest.length===0 ? 'empty':'everything ok';

    if(id===0 || id==="0"){
        response.data= REQUESTS;
    }else{

        response.data=numberRequest;
    }


    return response;

}
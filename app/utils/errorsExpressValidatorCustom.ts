
import { Result, ValidationError } from 'express-validator';

import { IResponse, itemResponseError, ResponseHttp } from "../helpers/response";



export const returnValidatorErrors = (errors: Result<ValidationError>): IResponse => {

    const res: IResponse = new ResponseHttp();

    const resultErrors: ValidationError[] = errors["errors"];
    const itemsError: itemResponseError[] = [];

    resultErrors.forEach(e => {
        itemsError.push({ criticality: 400, description: `${e.param} ${e.msg}` });
    })

    res.existsError = true;
    res.httpCode = 400;
    res.message = "the required parameters were not sent";
    res.errors = itemsError;

    return res;


}
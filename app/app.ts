import express,{Request,Response} from 'express';
import bodyParser from 'body-parser';
//load .env
import './config';


import IndexRoutes from './routes/index';
import { demoRoutes } from './utils/routesDemo';

const PORT:number=Number(process.env.PORT_HIGHTECH) || 5000;

const app:express.Application = express();

// app.use(bodyParser.urlencoded({extended:false}));
// app.use(bodyParser.json());

//from express v4.16 +
app.use(express.json())
app.use(express.urlencoded());

app.use('/api',IndexRoutes);

app.get('/',(req:Request,res:Response)=>{
    res.json(demoRoutes);
});




app.listen(PORT,()=>{
    console.log('RUNNING APP ON PORT '+PORT);
});





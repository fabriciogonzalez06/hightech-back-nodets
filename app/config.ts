import { config as configDotenv } from 'dotenv';
import { resolve } from 'path';

// const prueba=resolve(__dirname, '.env.development');
switch (process.env.NODE_ENV) {
    case "development":
        console.log("Environment is 'development'");
        configDotenv({
            path: resolve('./', '.env.development')
        });
        break;
    case "test":
        console.log("Environment is 'test'");
        
        configDotenv({
            path: resolve('./', '.env.test')
        });
        break;
    case "production":
        console.log("Environment is 'production'");
        configDotenv({
            path: resolve('./', '.env.production')
        });
        break;
    default:
        throw new Error(`NODE_ENV ${process.env.NODE_ENV} is not handled`);

}
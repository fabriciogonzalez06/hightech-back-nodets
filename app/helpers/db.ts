import msyql from 'mysql';
import { IResponse, ResponseHttp } from './response';

export default class DbHelper {

    private pool: msyql.Pool;

    private opt:msyql.PoolConfig;

    constructor(optDB?:msyql.PoolConfig) {

        if(optDB){
            this.opt={...optDB};
        }else{

            this.opt={
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE
            };
        }

        this.pool= this.createPool();
    }


    private createPool(){
        
      return msyql.createPool(this.opt);
    }

    async executeSp(nameSp: string, params: any): Promise<IResponse> {
        return new Promise((resolve, reject) => {

            // this.pool.on('connection',(connection)=>{
            //     console.log('open wiht ', nameSp);
            // })
            // this.pool.on('release',(connection)=>{
            //     console.log('Connection %d released', connection.threadId);
            // })

            const res: IResponse = new ResponseHttp();
            try {
                this.pool.getConnection(function (e: msyql.MysqlError, connection: msyql.PoolConnection) {
                    if (e) {
                        res.existsError = true;
                        res.httpCode = 500;
                        res.message = "unable to connect to database";
                        res.messageErrorInternal = e.message;
                        return resolve(res);
                    };

                    const queryOpt:msyql.QueryOptions={sql:nameSp,values:params }
                    connection.query(queryOpt, function(error,results) {

                        connection.release();

                        if (error) {
                            
                            res.existsError = true;
                            res.httpCode = 400;
                            res.message = error.message;
                            
                            return resolve(res);

                        }


                        res.data = results[0];
                        res.existsError = false;
                        res.httpCode = 200;
                        res.message = "successfully";
                        return resolve(res);
                    });



                });
            } catch (error) {
                res.existsError = true;
                res.httpCode = 500;
                res.message = "an internal error occurred";
                res.messageErrorInternal = error.message;
                resolve(res);
            } 
        });
    }

    public closePool() {
        if (this.pool) {
            this.pool.end();
        }
    }

}
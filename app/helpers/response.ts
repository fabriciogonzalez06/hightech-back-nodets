export interface IResponse {
    data: any;
    errors: IResponseItemError[];
    message: string;
    messageErrorInternal: string | any;
    existsError: boolean;
    httpCode: number;
}


export interface IResponseItemError {
    description: string;
    criticality: number;
}


export class ResponseHttp implements IResponse {

    public data: any;
    public errors: itemResponseError[];
    public message: string;
    public messageErrorInternal: string | any;
    public existsError: boolean;
    public httpCode: number;




    constructor(obj?: IResponse) {
        this.data = obj?.data || {};
        this.errors = obj?.errors || [];
        this.message = obj?.message || '';
        this.messageErrorInternal = obj?.messageErrorInternal || null;
        this.existsError = obj?.existsError || false;
        this.httpCode = obj?.httpCode || 200;
    }

    public static returnErrorInternal(errorInternal: string, message: string = "Ocurrio un error interno.", statusCode: number = 500): IResponse {
        const e: IResponse = {
            messageErrorInternal: errorInternal,
            httpCode: statusCode,
            existsError: true,
            message: message,
            data: {},
            errors: []
        };
        return e;
    }

    public returnCusmtomError(customError: IResponse): IResponse {

        const e = this;
        e.errors = customError.errors;
        e.existsError = true;
        e.httpCode = customError.httpCode;
        e.message = customError.message;
        e.messageErrorInternal = null;

        return e;
    }
    public static returnCusmtomErrorV2(message: string = "Ocurrio un error interno.", statusCode: number = 400): IResponse {

        const e: IResponse = {
            messageErrorInternal: null,
            httpCode: 400,
            existsError: true,
            message: message,
            data: {},
            errors: []
        };
        return e;
    }

}

export class itemResponseError implements IResponseItemError {
    criticality: number;
    description: string;

    constructor(itemError: IResponseItemError) {
        this.criticality = itemError.criticality;
        this.description = itemError.description;
    }
}

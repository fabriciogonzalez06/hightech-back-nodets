FROM node:14 as build

WORKDIR /app

COPY package*.json ./

RUN npm ci 

COPY tsconfig*.json ./

COPY src src 

RUN npm install typescript --save-dev 

RUN npx tsc -p ./tsconfig.json


### STAGE 2 delopy 


FROM node:14-alpine

ENV NODE_ENV=production 

WORKDIR /app 

RUN chown node:node .

USER NODE 

COPY package*.json ./

RUN npm i

COPY --from=build /app/dist ./

RUN ls 

EXPOSE 3000

ENTRYPOINT ["node","app.js"]
-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 161.35.141.24:3306
-- Tiempo de generación: 06-03-2021 a las 02:04:31
-- Versión del servidor: 10.4.17-MariaDB-1:10.4.17+maria~bionic
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hightech`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `tblempleados_obtener` (IN `p_id` INT)  begin
	

 	select 
 	id as UUID,
 	nombre,
 	correo,
 	telefono,
 	puesto,
 	edad,
 	notas,
 	fechaCreacion as fechaCreacion
 	from tbl_empleados te where id=case when (p_id=0 or p_id is null) then te.id else p_id end; 
	
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `tblempleado_insertar` (`p_nombre` VARCHAR(100), `p_correo` VARCHAR(100), `p_telefono` VARCHAR(20), `p_puesto` VARCHAR(30), `edad` TINYINT, `p_notas` VARCHAR(200))  begin
	

	
	
	declare v_idempleado int;
	
	declare exit handler for sqlexception
    begin
		rollback;
        resignal;
       
	end ;

	declare exit handler for sqlwarning
    begin
	    rollback;
        resignal;
		
     
    end ;

    
	
   if( exists(select 1 from tbl_empleados  where correo = p_correo) ) then 
   		signal SQLSTATE '45000' set message_text='Ya existe un empleado con ese correo';
   end if;


	start transaction;
	  insert into tbl_empleados(nombre,correo,telefono,puesto,edad,notas) 
	  values(
	 	p_nombre,
		p_correo,
		p_telefono,
		p_puesto,
		edad,
		p_notas 
	  );
	commit;

	select last_insert_id() into v_idempleado ; 
	/*
	select id,nombre,correo, puesto, edad,notas, fechaCreacion from tbl_empleados where id = v_idDemo;
	*/
	select v_idempleado as UUID;
	
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleados`
--

CREATE TABLE `tbl_empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `puesto` varchar(30) NOT NULL,
  `edad` tinyint(4) NOT NULL,
  `notas` varchar(200) NOT NULL,
  `fechaCreacion` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_empleados`
--

INSERT INTO `tbl_empleados` (`id`, `nombre`, `correo`, `telefono`, `puesto`, `edad`, `notas`, `fechaCreacion`) VALUES
(1, 'angel fabricio', 'angel@gmail.com', '123456', 'Programador', 23, 'sofware developer', '2021-03-05 19:35:53'),
(2, 'angel fabricio2', 'angel2@gmail.com', '123456', 'Programador', 23, 'sofware developer', '2021-03-05 19:54:22');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_empleados`
--
ALTER TABLE `tbl_empleados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_empleados`
--
ALTER TABLE `tbl_empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
